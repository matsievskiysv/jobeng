using Distributed
# addprocs()

@everywhere using Base.CoreLogging: Debug
@everywhere using Logging


@everywhere logger = Logging.SimpleLogger(stderr, Debug)
@everywhere Logging.global_logger(logger)

@everywhere push!(LOAD_PATH, pwd())
@everywhere using JobEngLW
using JobEng

@everywhere function a(x)
    println(x)
end

@everywhere function source()::Float64
    @debug "run source"
    return rand(Float64)
end
@everywhere function sink(a::Float64)
    @debug "run sink"
    println(a)
end

@everywhere function f1()
    println("f1")
    return 1, 2, 3
end

@everywhere function f2(x1)
    println("f2")
    return 2x1
end

@everywhere function f3(x1)
    println("f3")
    return 3x1
end

@everywhere function f4(x1)
    println("f4")
    return 4x1
end

@everywhere function f5(x1, x2, x3)
    println("f5")
    return 5x1 * x2 * x3
end

@everywhere function f6(x1)
    println("f6")
    return 6x1
end

@everywhere function f7(x1)
    println("f7")
    return 7x1
end

@everywhere function f8(x1)
    println("f8")
    for x in 1:10
        sleep(1)
        println("f8 - $x")
    end
    return 8x1
end

j = JobTree()
# push!(j, "source", source, output=["source one"], pure=false)
# push!(j, "source2", source, output=["source two"])
# push!(j, "middle1", source, input=["source one"], output=["middle one"])
# push!(j, "middle2", source, input=["middle one", "source two"], output=["middle two"])
# push!(j, "middle3", source, input=["middle one", "source one"], output=["middle three"])
# push!(j, "sink", sink, input=["source one", "source two", "middle three", "middle two"])
# push!(j, "sink2", sink, input=["source one"])
push!(j, "j1", f1, output=["o11", "o12", "o13"], pure=false)
push!(j, "j2", f2, input = ["o11"], output=["o2"])
push!(j, "j3", f3, input = ["o12"], output=["o3"])
push!(j, "j4", f4, input = ["o13"], output=["o4"])
push!(j, "j5", f5, input = ["o2", "o3", "o4"], output=["o5"])
push!(j, "j6", f6, input = ["o5"], output=["o6"])
push!(j, "j7", f7, input = ["o6"], output=["o7"])
push!(j, "j8", f8, input = ["o7"], output=["o8"])
# run!(j, "source")
# run!(j, "sink")
# run!(j, "sink2")
function doo()
    sleep(7)
    put!(j.comm_channel, 0)
end
# @async doo()
println(run!(j , "j8"))

# println(graph_all(j))
# println(graph_buffers(j, "j8"))
