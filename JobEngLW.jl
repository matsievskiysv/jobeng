module JobEngLW
using Distributed
using Debugger

export do_job, FunctionPacked

mutable struct FunctionPacked
    label::String
    func::Function
    inputs::Vector{Any}
    input_labels::Vector{String}
    outputs::Vector{Any}
    output_labels::Vector{String}
    old_hashes::Vector{Number}
    new_hashes::Vector{Number}
    pure::Bool
end

function do_job(funcs::Vector{FunctionPacked}, job_id::UInt, comm::RemoteChannel)::Vector{FunctionPacked}
    first_run = true
    for (i, f) in enumerate(funcs)
        if (!f.pure) | (f.old_hashes ≠ f.new_hashes)
            @debug "running function $(f.label)"
            if !first_run
                for (j1, n1) in enumerate(funcs[i].input_labels)
                    for (j2, n2) in enumerate(funcs[i-1].output_labels)
                        if n1 == n2
                            funcs[i].inputs[j1] = funcs[i-1].outputs[j2]
                            break
                        end
                    end
                end
            end
            output = f.func(f.inputs...)
            if typeof(output) == Nothing
                f.outputs = []
                f.new_hashes = []
            elseif length(output) == 1
                f.outputs = [output]
                f.new_hashes = [hash(output)]
            else
                f.outputs = [x for x in output]
                f.new_hashes = [hash(o) for o in output]
            end
            first_run = false
        else
            @debug "skipping function $(f.label)"
        end
    end
    put!(comm, job_id)
    return funcs
end

end
